# Super Smash Bros. Ultimate Models, exported for Blender 2.80+

[Blender](https://www.blender.org/) is used for working with the main files (.blend).
The models found here are near-straight imports from the original files, using my rewritten homemade Blender Python script at <https://gitlab.com/Worldblender/io_scene_numdlb/blob/master/SSBUlt_NUMDLB.py>. All of them are animation-ready, meaning that animation files can be applied to them right away. Custom animations can also be created, for those skilled enough to animate characters.

Every model is setup with only basic materials, which means that they have at least the color texture set up. Any meshes with two UV maps will also be setup correctly. Normal maps, PRM maps, and emissive maps are currently not yet set up, but they may be considered later on (although their textures will be there, they aren't used for now).

**All of the Blender files found here can be opened only with Blender 2.80 or later. Blender 2.79 and older cannot open them at all; attempting to do so will lead to nothing appearing in the 3D view.**

This repository uses Git Large File Storage (LFS), in order to keep the repository sizes more manageable. See https://git-lfs.github.com/ for more information and how to get the client.

Current status of models (appear only if they have files here):
**Check the file <./model-status-table.html> for the status of models appearing in this repository.**

### Related Resources
This project uses textures from <https://gitlab.com/Worldblender/smash-ultimate-textures>. Head over there for a pure texture dump, without other kinds of files.

* Project thread: <https://www.vg-resource.com/thread-34836.html>

* Direct folder (MEGA.nz): <https://mega.nz/#F!AAwWzCjT!Zd5kKpuQcE647DSRtpFShw>

* PNG textures: <https://gitlab.com/Worldblender/smash-ultimate-textures>

## Import Workflow
1. Delete the default cube and point light. Leave the camera intact; it may be useful for something else.

2. Navigate to the directory with the desired NUMDLB file, and use the NUMDLB Importer to import it.

3. Change the Viewport Shading method to Material Preview. This method allows seeing two UV maps at once, especially allowing to simultaneously see most fighters' eye white and iris.

4. Switch to Front view, and Frame All to have the entire model fit within the 3D Viewport. Optionally make the armature not draw in front of other objects, so that the meshes can be better seen.

5. Save the imported model to a Blender file, where it will be saved to a same-named location (in this repository) as that of the original NUMDLB file.

6. (Optional, but recommended) Open `cleanup-meshes.py` to hide most, if not all of, the facial expressions, as well as to change the texture locations to be relative to the saved Blender file.

7. (Optional) Play around with the studio lighting setup by switching between any of the 8 studio light images. The default studio light for all models is Forest.

8. (Optional) Position the camera to the active view with Align Active Camera to View. Otherwise, it can left at its default position.

## Dealing with shader-internal images
A few models do not use external textures for a few of their materials. None of the playable fighter characters use such materials, but a few other non-playable characters (a few assist fighters/trophies and stage bosses) do. Such materials mainly appear as part of making a certain material transparent, like glass, when a dedicated texture is not needed.

As these images not found in here or the texture repository, they can be replaced with a Blender-generated image. Known shader-internal images, and replacements:

* /common/shader/sfxPBS/default_black -> Source: Generated, Type: Blank, Color (hex): #000000

* /common/shader/sfxPBS/default_White -> Source: Generated, Type: Blank, Color (hex): #FFFFFF

* /common/shader/sfxPBS/default_Params (PRM only) -> No known replacement yet
